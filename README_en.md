#### Banner AD
1. Insert the DIV tag in the location where you want the ad to be exposed.
2. Declares and creates a Developer Center ad object.
3. Make default settings to run ads.
4. The section that connects the exposure of an ad by connecting the area of DIV ID.

#### Video AD
1. Insert a video tag where you want to expose the ad.
2. Declares and creates an ad object.
   2. Set the mode of the video ad (INJECTION or OVERLAY)
   2. Specify the Video Tag ID where the ad is exposed.
3. Make default settings to run ads.
   3. Change the issued Publisher_Code setting.
   3. Change the media code (media_code) issued.
   3. Change the section code (Section_Code) issued.
4. You can change the advertising options described in 2-2.
